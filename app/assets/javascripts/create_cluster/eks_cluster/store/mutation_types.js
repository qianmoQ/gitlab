export const SET_REGION = 'SET_REGION';
export const SET_VPC = 'SET_VPC';
export const SET_KEY_PAIR = 'SET_KEY_PAIR';
export const SET_SUBNET = 'SET_SUBNET';
export const SET_ROLE = 'SET_ROLE';
